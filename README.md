![stability-workinprogress](https://img.shields.io/badge/stability-work_in_progress-lightgrey.svg)
![internaluse-green](https://img.shields.io/badge/Internal%20use%3A-stable-green.svg)
![issues-open](https://img.shields.io/badge/issues-open-green.svg)

# RATIONALE #

* Internal branding gathered to apply to multiple digital strategies: mobile apps, webinars, social media etc. with a focus in accessibility.
* This repo is a living document that will grow and adapt over time

![graphics.jpg](https://i.ibb.co/NtyJywt/333.gif)

### What is this repository for? ###

* Quick summary
    - Branding digital assets for visual identity
      ![graphics.jpg](https://i.ibb.co/SX5vF9h/ezgif-com-gif-maker.gif)

* Version 1.01

### How do I get set up? ###

* Summary of set up
    - Check [colophon.md](https://bitbucket.org/imhicihu/branding/src/master/colophon.md)
* Deployment instructions
    - Check [logotype accesibility.md](https://bitbucket.org/imhicihu/branding/src/master/logotype_accesibility.md)
    - Check [cromatic aberrations.md](https://bitbucket.org/imhicihu/branding/src/master/cromatic_aberrations.md)
    - [Preliminary logos](https://bitbucket.org/imhicihu/branding/issues/5/alternative-logos)

### Source ###

* Check them on [here](https://bitbucket.org/imhicihu/branding/src)

### Issues ###

* Check them on [here](https://bitbucket.org/imhicihu/branding/issues)

### Changelog ###

* Please check the [Commits](https://bitbucket.org/imhicihu/branding/commits/) section for the current status

### Who do I talk to? ###

* Repo owner or admin
    - Contact `imhicihu` at `gmail` dot `com`
* Other community or team contact
    - Contact is _enable_ on the [board](https://bitbucket.org/imhicihu/branding/addon/trello/trello-board) of this repo. (You need a [Trello](https://trello.com/) account)

### Code of Conduct

* Please, check our [Code of Conduct](https://bitbucket.org/imhicihu/branding/src/master/code_of_conduct.md)


### Legal ###

* All trademarks are the property of their respective owners.

### License ###

* The content of this project itself is licensed under the MIT license.