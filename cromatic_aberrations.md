# Original
![graphics.jpg](https://bitbucket.org/imhicihu/branding/issues/attachments/2/imhicihu/branding/1631323077.07/2/Untitled.jpg)
#

# Protanomaly
![graphics.jpg](https://bitbucket.org/imhicihu/branding/issues/attachments/2/imhicihu/branding/1631315355.0/2/Protanomaly.jpg)
#

# Deuteranomaly
![graphics.jpg](https://bitbucket.org/imhicihu/branding/issues/attachments/2/imhicihu/branding/1631315354.91/2/Deuteranomaly.jpg)
#

# Tritanomaly
![graphics.jpg](https://bitbucket.org/imhicihu/branding/issues/attachments/2/imhicihu/branding/1631315355.18/2/Tritanomaly.jpg)
#

# Protanopia
![graphics.jpg](https://bitbucket.org/imhicihu/branding/issues/attachments/2/imhicihu/branding/1631315355.09/2/Protanopia.jpg)
#

# Deuteranopia
![graphics.jpg](https://bitbucket.org/imhicihu/branding/issues/attachments/2/imhicihu/branding/1631321979.33/2/Deuteranopia.jpg)
#

# Tritanopia
![graphics.jpg](https://bitbucket.org/imhicihu/branding/issues/attachments/2/imhicihu/branding/1631315355.28/2/Tritanopia.jpg)
#

# Achromatopsia
![graphics.jpg](https://bitbucket.org/imhicihu/branding/issues/attachments/2/imhicihu/branding/1631315354.63/2/Achromatopsia.jpg)
#

# Blue Cone Monochromacy
![graphics.jpg](https://bitbucket.org/imhicihu/branding/issues/attachments/2/imhicihu/branding/1631315354.81/2/Blue_Cone_Monochromacy.jpg)
#